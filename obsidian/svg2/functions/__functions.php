<?php
function sidebar_checkbox($id, $text, $disable = false) {
	global $config;
	?>
	<div class="clearfix">
	   <p class="pull-left<?php if ($disable) echo ' switch-disable'; ?>"><?php echo $text; ?></p>
	   <div class="pull-right">
		  <label class="switch<?php if ($disable) echo ' switch-disable'; ?>">
			 <input id="chk-<?php echo $id; ?>" type="checkbox" <?php if ($config[$id]=='y') echo ' checked'; ?><?php if ($disable) echo ' disabled'; ?>>
			 <span></span>
		  </label>
	   </div>
	</div>
	<?php
}

function sidebar_inputtext($id, $text, $disable = false, $step = 1, $min = 0, $max = 100) {
	global $config;
	?>
	<div class="clearfix">
	   <p class="pull-left <?php if ($disable) echo 'switch-disable'; ?>"><?php echo $text; ?></p>
	   <div class="pull-right">
		  <label class="textfield <?php if ($disable) echo 'switch-disable'; ?>">
			 <input id="chk-<?php echo $id; ?>" type="number" value="<?php echo $config[$id]; ?>" step="<?php echo $step; ?>" min="<?php echo $min; ?>" max="<?php echo $max; ?>" class="form-control onestep" <?php if ($disable) echo ' disabled'; ?> />
			 <span></span>
		  </label>
	   </div>
	</div>
	<?php
}

function sidebar_dropdown($id, $text, $data) {
	global $config;
	?>
	<div class="clearfix">
	   <p class="pull-left"><?php echo $text; ?></p>
	   <div class="pull-right">
		  <label class="btn-group dropup">
			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width: 84px; padding: 0;color: #3a3f51;font-size: 14px;font-weight: bold;"><div id="text-<?php echo $id; ?>" style="overflow: hidden; width: 60px; float: left;"><?php echo $config[$id]; ?></div><span class="caret" style="border-right: 4px solid transparent; border-left: 4px solid transparent;"></span></button>
			<ul id="chk-<?php echo $id; ?>" class="dropdown-menu dropdown-menu-right sidebar-item-dropdown" style="min-width: inherit;">
				<?php foreach ($data as $item) { ?>
				<li><a href="#"><?php echo $item; ?></a></li>
				<?php } ?>
			</ul>
		  </label>
	   </div>
	</div>
	<?php
}

// function put_ini_file($file, $array){
// 	$str="";
// 	if (is_array($array)) {
// 		foreach ($array as $k => $v){
// 			if (!is_array($v)){
// 				$str.="$k=\"$v\"".PHP_EOL;
// 			}
// 		}
// 		foreach ($array as $k => $v){
// 			if (is_array($v)){
// 				$str.="[$k]".PHP_EOL;
// 				$str.=put_ini_file("",$v);
// 				$str.=PHP_EOL;
// 			}
// 		}
// 	}
// 	if($file) return file_put_contents($file,$str);
// 	else return $str;
// }

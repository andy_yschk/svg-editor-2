<?php
header('Content-type: application/json');

$data = $_POST['data'];
$fname = $_POST['filename']; // filename name
$mime_content_type = $_POST['mime_content_type'];
if(!empty($data)){
    if ($mime_content_type === 'application/json' && isJson($_POST['data'])){
        $file = fopen("../../../dashboard/images/svgeditorexport/projects/" .$fname . ".dia.json", 'w');
        fwrite($file, $data);
        fclose($file);
        echo '{"success":"json file sucessfully saved"}';
    } else if ($mime_content_type === 'image/svg+xml' /*&& simplexml_load_string($_POST['data'])*/){
        $file = fopen("../../../dashboard/images/svgeditorexport/" .$fname . ".svg", 'w');
        fwrite($file, $data);
        fclose($file);
        echo '{"success":"svg file sucessfully saved"}';
    } else {
        echo '{"error":"unrecognized file format"}';
    }
}

function isJson($string) {
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
}
?>
<?php
$default_config = parse_ini_file(__DIR__."/../config/config.ini", true);
$config = parse_ini_file(__DIR__."/../config/svg.ini", true);
$config = ini_merge($default_config, $config);

include_once('/opt/ozona/obsidian/config/encrypt.php');
$config = configs_decrypt($config);

function ini_merge ($config_ini, $custom_ini) {
  foreach ($custom_ini AS $k => $v){
    if (is_array($v)){
      $config_ini[$k] = ini_merge($config_ini[$k], $custom_ini[$k]);
    }else{
      $config_ini[$k] = $v;
    }
  }
  return $config_ini;
}

require_once(__DIR__."/../api/Obsidian/Tools/Database.php");
$linkObsidian = new Obsidian\Tools\Database($config['mysql_host'], $config['mysql_user'], $config['mysql_pasw'],$config['mysql_db']);
    
if (isset($_REQUEST['mode']) && $_REQUEST['mode'] == 'getLinkDependencies') {
    $IDs = explode(',', $_REQUEST['IDs']);
    $model_IDs = explode(',', $_REQUEST['model_IDs']);
    $output = array();
    for ($i = 0; $i < count($IDs); $i++) {
        $id = $IDs[$i];
        $service = $linkObsidian->fetch_all_array("SELECT * FROM bsm_service_tree t WHERE ((t.ci_monitor!='' AND t.ci_monitor='".$linkObsidian->real_escape_string($id)."') OR (t.ci_monitor='' AND CAST(t.id AS CHAR(10))='".$linkObsidian->real_escape_string($id)."')) AND t.ci_type NOT IN ('monitor','availability','capacity','kpi','dependencies2','dependencies3','synthetictransaction','servicedesk')");
        $output[$model_IDs[$i]] = array();
        if (isset($service[0])) {
            $depLink = $linkObsidian->fetch_all_array("SELECT t2.* FROM bsm_service_tree t1 JOIN bsm_service_tree t2 ON t2.id_bsm_service = t1.ci_monitor WHERE t1.id_bsm_service=" . $service[0]['id_bsm_service'] . " AND t1.ci_type IN ('dependencies2', 'dependencies3') AND t2.ci_type = 'service'");
            foreach ($depLink as $dep) {
                if ($dep['ci_monitor'] != '') {
                    $dep_id = $dep['ci_monitor'];
                } else {
                    $dep_id = $dep['id'];
                }
                $key = array_search($dep_id, $IDs);
                if ($key !== false) {
                    $output[$model_IDs[$i]][] = $model_IDs[$key];
                }
            }
        }
    }
    echo json_encode($output);
} else if (isset($_REQUEST['mode']) && $_REQUEST['mode'] == 'checkLinkDependency') {
    $target = $linkObsidian->fetch_all_array("SELECT * FROM bsm_service_tree t WHERE ((t.ci_monitor!='' AND t.ci_monitor='".$linkObsidian->real_escape_string($_REQUEST['target'])."') OR (t.ci_monitor='' AND CAST(t.id AS CHAR(10))='".$linkObsidian->real_escape_string($_REQUEST['target'])."')) AND t.ci_type NOT IN ('monitor','availability','capacity','kpi','dependencies2','dependencies3','synthetictransaction','servicedesk')");
    $source = $linkObsidian->fetch_all_array("SELECT * FROM bsm_service_tree t WHERE ((t.ci_monitor!='' AND t.ci_monitor='".$linkObsidian->real_escape_string($_REQUEST['source'])."') OR (t.ci_monitor='' AND CAST(t.id AS CHAR(10))='".$linkObsidian->real_escape_string($_REQUEST['source'])."')) AND t.ci_type NOT IN ('monitor','availability','capacity','kpi','dependencies2','dependencies3','synthetictransaction','servicedesk')");
    if (isset($target[0]) && isset($source[0])) {
        $depLink = $linkObsidian->fetch_all_array("SELECT * FROM bsm_service_tree t1 WHERE t1.id_bsm_service=" . $target[0]['id_bsm_service'] . " AND t1.ci_type IN ('dependencies2', 'dependencies3') AND t1.ci_monitor = '".$linkObsidian->real_escape_string($source[0]['id_bsm_service'])."'");
        if (isset($depLink[0])) {
            echo 1;
        } else {
            echo 0;
        }
    } else {
        echo 0;
    }
} else if (isset($_REQUEST['mode']) && $_REQUEST['mode'] == 'checkLinksDependencies') {
    $output = array();
    $sources = explode(',', $_REQUEST['source']);
    $targets = explode(',', $_REQUEST['target']);
    $model_sources = explode(',', $_REQUEST['model_source']);
    $model_targets = explode(',', $_REQUEST['model_target']);
    for ($i = 0; $i < count($sources); $i++) {
        if ($sources[$i] == '' || $targets[$i] == '') continue;
        $target = $linkObsidian->fetch_all_array("SELECT * FROM bsm_service_tree t WHERE ((t.ci_monitor!='' AND t.ci_monitor='".$linkObsidian->real_escape_string($targets[$i])."') OR (t.ci_monitor='' AND CAST(t.id AS CHAR(10))='".$linkObsidian->real_escape_string($targets[$i])."')) AND t.ci_type NOT IN ('monitor','availability','capacity','kpi','dependencies2','dependencies3','synthetictransaction','servicedesk')");
        $source = $linkObsidian->fetch_all_array("SELECT * FROM bsm_service_tree t WHERE ((t.ci_monitor!='' AND t.ci_monitor='".$linkObsidian->real_escape_string($sources[$i])."') OR (t.ci_monitor='' AND CAST(t.id AS CHAR(10))='".$linkObsidian->real_escape_string($sources[$i])."')) AND t.ci_type NOT IN ('monitor','availability','capacity','kpi','dependencies2','dependencies3','synthetictransaction','servicedesk')");
        if (isset($target[0]) && isset($source[0])) {
            $depLink = $linkObsidian->fetch_all_array("SELECT * FROM bsm_service_tree t1 WHERE t1.id_bsm_service=" . $target[0]['id_bsm_service'] . " AND t1.ci_type IN ('dependencies2', 'dependencies3') AND t1.ci_monitor = '".$linkObsidian->real_escape_string($source[0]['id_bsm_service'])."'");
            if (!isset($depLink[0])) {
                $output[] = array($model_sources[$i], $model_targets[$i], $source[0]['id_bsm_service'] . '_' . $target[0]['id_bsm_service']);
            }
        } else {
            $output[] = array($model_sources[$i], $model_targets[$i], $source[0]['id_bsm_service'] . '_' . $target[0]['id_bsm_service']);
        }
    }
    echo json_encode($output);
}

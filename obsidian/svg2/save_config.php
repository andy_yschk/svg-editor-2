<?php
include_once("../dashboard/functions/__functions.php");
include_once("functions/__functions.php");
if (isset($_REQUEST['set_element']) && isset($_REQUEST['value']) && isset($_REQUEST['file'])){
	$cfgFile = parse_ini_file(__DIR__.'/../config/'.$_REQUEST['file'].'.ini',true);
	if ($_REQUEST['set_element']!='') $cfgFile[$_REQUEST['set_element']] = $_REQUEST['value'];
	put_ini_file(__DIR__.'/../config/'.$_REQUEST['file'].'.ini', $cfgFile);
	echo 'OK';
}

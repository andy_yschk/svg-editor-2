<?php
include_once("functions/__functions.php");
include_once("../dashboard/functions/__functions.php");
include_once("../dashboard/header.php");
$default_config = parse_ini_file(__DIR__."/../config/config.ini", true);
$config = parse_ini_file(__DIR__."/../config/svg2.ini", true);
$config = ini_merge($default_config, $config);

checkAuthPermission();

?>
<!DOCTYPE html>
<html lang="en" class="csstransforms3d">
<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <title>Map Editor</title>
   <link rel="stylesheet" href="includes/css/bebas-neue.css">
   <link rel="stylesheet" href="includes/fontawesome/css/font-awesome.min.css">
   <link rel="stylesheet" href="includes/line-awesome/css/line-awesome.css">
   <link rel="stylesheet" href="includes/simple-line-icons/css/simple-line-icons.css">
	<link rel="stylesheet" href="includes/jqueryfiletree/jqueryFileTree.css">
   <link rel="stylesheet" href="includes/jquery-ui/jquery-ui.css">
   
   <link rel="stylesheet" href="includes/jquery-ui-iconfont/jquery-ui-1.12.icon-font.min.css" />
   <link rel="stylesheet" href="includes/css/bootstrap.css" id="bscss">
   <link rel="stylesheet" href="includes/css/app.css" id="maincss">
   <link rel="stylesheet" href="includes/css/custom_bootstrap.css" id="custom">
   <link rel="stylesheet" href="css/joint.css" />
   <link rel="stylesheet" href="css/joint.ui.freeTransform.min.css">
   <link rel="stylesheet" href="css/joint.ui.stencil.min.css">
   <link rel="stylesheet" href="css/joint.ui.halo.min.css">
   <link rel="stylesheet" href="css/joint.ui.contextToolbar.min.css">
   <link rel="stylesheet" href="css/joint.ui.popup.min.css">
   <link rel="stylesheet" href="css/joint.ui.selectBox.min.css">
   <link rel="stylesheet" href="css/joint.ui.selectionView.min.css">
   <link rel="stylesheet" href="css/joint.ui.textEditor.min.css">
   <link rel="stylesheet" href="css/style.css?ver=<?php echo time();?>">
<body class="layout-h layout-boxed">
<div class="wrapper">
<!-- top navbar-->
<header class="topnavbar-wrapper">
      <!-- START Top Navbar-->
      <nav role="navigation" class="navbar navbar-inverse">
      <!-- START navbar header-->
      <div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               <a href="#" class="navbar-brand">
               </a>
            </div>
            <!-- END navbar header-->
            <!-- START Nav wrapper-->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
               <!-- START Right Navbar-->
               <ul class="nav navbar-nav navbar-right" id="myTabs">  
                  <li>
                     <a href="#" onclick="showHintDialog();" style="margin-right: 0px;">
                        <em class="icon-info"></em>
                     </a>
                  </li>    
                  <li>
                     <a href="#" onclick="svgScript();" style="margin-right: 0px;">
                        <em class="la la-code"></em>
                     </a>
                  </li>            
                  <li>
                     <a href="#" onclick="newProject();" style="margin-right: 0px;">
                        <em class="icon-doc"></em>
                     </a>
                  </li>
                  <li>
                     <a href="#" onclick="openJSONProject();" style="margin-right: 0px;">
                        <em class="fa fa-folder-open-o"></em>
                     </a>
                  </li>     
                  <li>
                     <a href="#" onclick="saveProjectToJSON();" style="margin-top: -1px; margin-right: 0px;">
                        <em class="fa fa-save"></em>
                     </a>
                  </li>
                  <li>
                     <a href="#" onclick="exportSVG();">
                        <em class="icon-picture"></em>
                     </a>
                  </li>
                  <!-- START Alert menu-->
                  <li class="dropdown dropdown-list">
                     <a href="#" data-toggle="dropdown">
                        <em class="icon-bell"></em>
                        <div class="label label-danger"></div>
                     </a>
                     <!-- START Dropdown menu-->
                     <ul class="dropdown-menu animated flipInX">
                        <li>
                           <!-- START list group-->
                           <div class="list-group">
                              <!-- last list item -->
                              <a href="#" class="list-group-item">
                                 <small>No notification</small>
                                 <span class="label label-danger pull-right"></span>
                              </a>
                           </div>
                           <!-- END list group-->
                        </li>
                     </ul>
                     <!-- END Dropdown menu-->
                  </li>
                  <!-- END Alert menu-->
                  <!-- START Boxed button-->
                  <li class="visible-lg">
                     <a href="#" data-toggle-state="layout-boxed">
                        <em class="la la-arrows-h"></em>
                     </a>
                  </li>
                  <!-- END Boxed menu-->
                  <!-- Fullscreen-->
                  <li class="visible-lg">
                     <a href="#" onclick="fullscreen();">
                        <em class="la la-expand"></em>
                     </a>
                  </li>
                  <!-- START Offsidebar button-->
                  <li>
                     <a id="awsidebar" href="#" data-toggle-state="offsidebar-open" data-no-persist="true">
                        <em class="icon-notebook"></em>
                     </a>
                  </li>
                  <!-- END Offsidebar menu-->
               </ul>
               <!-- END Right Navbar-->
            </div>
            <!-- END Nav wrapper-->
         </nav>
         <!-- END Top Navbar-->
      </header>
      <!-- offsidebar-->
      <aside class="offsidebar hide">
         <!-- START Off Sidebar (right)-->
         <nav>
            <div role="tabpanel">
               <!-- Tab panes-->
               <!-- Nav tabs-->
               <ul role="tablist" class="nav nav-tabs nav-justified">
                  <li role="presentation" class="active">
                     <a href="#app-config" aria-controls="app-config" role="tab" data-toggle="tab">
                        <em class="icon-equalizer fa-lg"></em>
                     </a>
                  </li>
               </ul>
               <div class="tab-content">
                  <div id="app-config" role="tabpanel" class="tab-pane fade in active">
                     <h3 class="text-center text-thin">EDITOR SETTINGS</h3>
                     <div class="p">
						      <h4 class="text-muted text-thin">Grid</h4>
                        <?php
                           sidebar_checkbox('showGrid', 'Show grid');
                           sidebar_inputtext('gridSize', 'Grid size', false, 1, 1, 20);
                           sidebar_inputtext('primaryMeshThickness', 'Primary mesh thickness', true, 0.1, 0, 2);
                           sidebar_inputtext('secondaryMeshThickness', 'Secondary mesh thickness', true, 0.1, 0, 1);
                           sidebar_inputtext('secondaryMeshScaleFactor', 'Secondary mesh scale factor', true, 1, 0, 10);
                           sidebar_checkbox('showSnaplines', 'Show Snaplines');
                           sidebar_checkbox('showHint', 'Show hint by default');
                        ?>
                     </div>
                  </div>
               </div>
            </div>
         </nav>
         <!--END Off Sidebar (right)-->
      </aside>
      <section>  
	<div class="tab-content">
		<div class="tab-pane active" id="tab-id">
			<img id="tab-loader" src="icons/ajax-loader.svg" style="position: absolute; top: 50%; left: 50%; display: none;"/>
			<div id="paper-container">
            <div id="paper-container-zoomable">
            </div>
         </div>
			<div id="toolbar-container">
			   <button class="btn-zoom-in" data-tooltip="Zoom In"><i class='icon-magnifier-add'></i></button>
			   <button class="btn-zoom-reset" data-tooltip="Zoom To Fit"><i class='icon-size-actual'></i></button>
			   <button class="btn-zoom-out" data-tooltip="Zoom Out"><i class='icon-magnifier-remove'></i></button>
			   <button class="btn-undo" onclick="commandManager.undo();" data-tooltip="Undo"><i class='icon-action-undo'></i></button>
			   <button class="btn-redo" onclick="commandManager.redo();" data-tooltip="Redo"><i class='icon-action-redo'></i></button> 
			</div>
			<div id="inspector-container">
			</div>
         <div id="stencil-container">
         </div>
		</div>
	</div>
</section>
<!-- Page footer-->
<footer class="text-right">
   <div class="container">
      <div class="row">
         <div class="col-lg-12">
            <span>
               <img src='/obsidian/console/images/obsidian.svg' alt='Obsidian Logo' width=16>
                         Obsidian <?php echo $config['obsidianVersion']; ?>
                         &copy; <?php echo date('Y'); ?>
               <a href=http://www.obsidiansoft.com target=_blank>Obsidian Soft</a>
            </span>
         </div>
      </div>
   </div>
</footer>
</div>
    <script src="includes/ace/src-min-noconflict/ace.js"></script>
    <script src="js/exportSVG.js"></script>
    <script src="includes/jquery/dist/jquery-3.3.1.min.js"></script>
    <script src="includes/bootstrap/dist/js/bootstrap.js"></script>
    <script src="includes/jQuery-Storage-API/jquery.storageapi.js"></script>
    <script src="includes/jqueryfiletree/jqueryFileTree.js" type="text/javascript"></script>
    <script src="includes/jquerypanzoom/panzoom.js"></script>
	 <script src="../includes/rappid/node_modules/lodash/index.js"></script>
  	 <script src="../includes/rappid/node_modules/backbone/backbone.js"></script>
    <script src="includes/js/app.js"></script>
    <script src="js/joint.min.js"></script>
    <script src="js/joint.ui.freeTransform.min.js"></script>
    <script src="js/joint.ui.stencil.min.js"></script>
    <script src="js/joint.ui.halo.min.js"></script>
    <script src="js/joint.ui.snaplines.min.js"></script>
    <script src="js/joint.ui.clipboard.min.js"></script>
    <script src="js/joint.ui.contextToolbar.min.js"></script>
    <script src="js/joint.ui.popup.min.js"></script>
    <script src="js/joint.ui.selectBox.min.js"></script>
    <script src="js/joint.ui.selectionView.min.js"></script>
    <script src="js/joint.dia.command.min.js"></script>
    <script src="js/joint.ui.textEditor.min.js"></script>
    <script src="includes/jquery-ui/jquery-ui.js"></script>
    <script src="js/editor.js?ver=<?php echo time();?>"></script>
    <script type="text/javascript">
      var updateGraphView;
      // get only config strings, no arrays
      var config_ini = JSON.parse(`{<?php 
         foreach ($config AS $k => $v){
               echo '"'.$k.'":"'.$v.'",';
            }?>
      "":""}`);

      for (prop in config_ini){
         if (config_ini[prop] == "Array" || config_ini[prop] == ""){
            delete config_ini[prop];
         } else {
            updateGraphView(prop, config_ini[prop]);
         }
      }

      tmp = parseFloat($('.navbar').css('margin-bottom'));
      
      
      $('.switch input, .textfield input').on('change', function (e) {
            let type = $(e.target).attr('type');
            tmp = e.target.id.split('-');
            if (tmp.length==2) {
               prefix = tmp[0];
               id = tmp[1];

            switch (type) {
               case 'checkbox':
                     if (e.target.checked) value='y';
                     else value='n';
                  break;
               case 'number':
                     value = e.target.value;
                  break;
               default:
                     value = undefined;
                  break;
            }
               
            if (!value) return;

            if (updateGraphView){
               updateGraphView(id, value);
            }
               if (prefix=='chk') {
                  $.ajax({
                     url: "save_config.php?set_element="+id+"&value="+value+"&file=svg2",
                  });
               }
            }
         });
               
         // $('.sidebar-item-dropdown').on('click', function (e) {
         //    console.log(e);
         //    tmp = e.currentTarget.id.split('-');
         //    if (tmp.length==2) {
         //       prefix = tmp[0];
         //       id = tmp[1];
         //       value=e.target.text;
         //       if (prefix=='chk') {
         //          $('#text-'+id).text(value);
         //          combotype = value;
         //          $.ajax({
         //             url: "save_config.php?set_element="+id+"&value="+value+"&file=svg2",
         //          });
         //       }
         //    } 
         // })

         function fullscreen() {
            if(document.fullscreen){
               closeFullscreen();
            } else {
               var elem = document.documentElement;
               if (elem.requestFullscreen) {
                  elem.requestFullscreen();
               } else if (elem.mozRequestFullScreen) { /* Firefox */
                  elem.mozRequestFullScreen();
               } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
                  elem.webkitRequestFullscreen();
               } else if (elem.msRequestFullscreen) { /* IE/Edge */
                  elem.msRequestFullscreen();
               }
            }
         }

         function closeFullscreen() {
            var elem = document.documentElement;
            if (document.exitFullscreen) {
               document.exitFullscreen();
            } else if (document.mozCancelFullScreen) { /* Firefox */
               document.mozCancelFullScreen();
            } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
               document.webkitExitFullscreen();
            } else if (document.msExitFullscreen) { /* IE/Edge */
               document.msExitFullscreen();
            }
         }
</script>
</body>
</html>
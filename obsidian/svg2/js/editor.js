var canvasWidth = 4000;
var canvasHeight = 4000;
var cloningModel;
var currentFileName;
var startCloning = false;
var startLinking = false;
// detect pressed key to combine with mouse click
var pressedKey = undefined;
var startX, startY;
var newLink;
// initial selection points
var ppx0, ppy0;
var selectedProjectFile;
const noTextValue = '[[no text]]';

// unique IDs
var ExportIDList = {
    ExportIDs: new Set(),
    LastIDNum: 0,
    LastAddedID: 0,
    NewID: function (id) {
        if (id && !this.ExportIDs.has(id)) {
            this.ExportIDs.add(id + "");
            this.LastAddedID = id;
            return id;
        } else {
            this.ExportIDs.add(++this.LastIDNum + "");
            this.LastAddedID = this.LastIDNum + "";
            return this.LastAddedID;
        }
    },
    UpdateID: function (oldid, newid) {
        this.ExportIDs.delete(oldid + "");
        this.NewID(newid);
        return this.LastAddedID;
    },
    AllIDs: function () {
        return this.ExportIDs;
    }
};

// function to embed into exported SVG
let hoverEffectScript = function () {
    document.querySelectorAll("[id^=service-]").forEach((el) => {
        const serviceId = el.id.replace(/^service-/, "");
        el.addEventListener("mouseenter", () => {
            document.querySelectorAll("[id^=link-]").forEach((linkEl) => {
                if (linkEl.id.includes("link-" + serviceId + "-") ||
                    linkEl.id.endsWith("-" + serviceId)) {
                    linkEl.classList.add("selected_line");
                } else {
                    linkEl.classList.add("deselected_line");
                }
            });
        });
        el.addEventListener("mouseleave", () => {
            document.querySelectorAll("[id^=link-]").forEach((linkEl) => {
                linkEl.classList.remove("selected_line");
                linkEl.classList.remove("deselected_line");
            });
        });
    });
}

var clipboard = new joint.ui.Clipboard({ useLocalStorage: true });

// custom link definition
var Link = joint.shapes.devs.Link.define('Link',
    {
        attrs: {
            //'.connection': { 'stroke': '#000000', 'stroke-width': '2', 'stroke-linecap': 'round', 'stroke-miterlimit': '4', 'stroke-dasharray': '0,5' },
            '.connection': { 'stroke': '#000000', 'stroke-width': '1', 'stroke-linecap': 'round' },
            '.marker-target': { fill: 'white', d: 'M 10 0 L 0 4 L 10 8 z' },
            '.tool-options': { display: 'none' },
        },
        router: {
            name: 'customRouter',
            args: {
                excludeTypes: ['Floor', 'Popup'],
                startDirections: ['bottom'],
                endDirections: ['top'],
                step: 10,
            }
        }, // manhattan // orthogonal // metro
        connector: {
            name: 'rounded',
            args: {
                radius: 5
            }
        }
    });

// custom link router
joint.routers.customRouter = function (vertices, args, linkView) {

    // vertices = joint.routers.manhattan(vertices, args, linkView);
    // let customVertices = joint.util.toArray(vertices).map(g.Point);

    vertices = [].map(g.point);
    const sx = linkView.sourceAnchor.x;
    const sy = linkView.sourceAnchor.y;
    const tx = linkView.targetAnchor.x;
    const ty = linkView.targetAnchor.y;
    let dy = (linkView.sourceBBox.center().x - sx) * (sx > tx ? -1:1) - (linkView.targetBBox.center().x - tx) * (sx > tx ? 1:-1) * 1.7;

    if (sy < ty) {
        if (Math.abs(sx - tx) > 80) {
            if (sx > tx) {
                vertices.push(g.Point(sx, sy + (ty - sy) / 1.5 - dy));
                vertices.push(g.Point(tx, sy + (ty - sy) / 1.5 - dy));
                //  console.log("to front right");
            } else {
                vertices.push(g.Point(sx, sy + (ty - sy) / 1.5 - dy));
                vertices.push(g.Point(tx, sy + (ty - sy) / 1.5 - dy));
                //  console.log("to front left");
            }
        } else {
            if (sx > tx) {
                vertices.push(g.Point(sx, sy + (ty - sy) / 1.5 - dy));
                vertices.push(g.Point(tx, sy + (ty - sy) / 1.5 - dy));
                //  console.log("to front right (close)");
            } else {
                vertices.push(g.Point(sx, sy + (ty - sy) / 1.5 - dy));
                vertices.push(g.Point(tx, sy + (ty - sy) / 1.5 - dy));
                //  console.log("to front left (close)");
            }
        }
    }
    else {
        if (Math.abs(sx - tx) > 80) {
            if (sx > tx) {
                vertices.push(g.Point(sx, sy + 20 - dy));
                vertices.push(g.Point(sx - 40, sy + 20 - dy));
                vertices.push(g.Point(sx - 40, ty - 20 - dy));
                vertices.push(g.Point(tx, ty - 20 - dy));
                //  console.log("to back right");
            } else {
                vertices.push(g.Point(sx, sy + 20 - dy));
                vertices.push(g.Point(sx + 40, sy + 20 - dy));
                vertices.push(g.Point(sx + 40, ty - 20 - dy));
                vertices.push(g.Point(tx, ty - 20 - dy));
                //  console.log("to back left");
            }
        } else {
            if (sx > tx) {
                vertices.push(g.Point(sx, Math.max(ty + 100, sy + 20) - dy));
                vertices.push(g.Point(tx - 40, Math.max(ty + 100, sy + 20) - dy));
                vertices.push(g.Point(tx - 40, ty - 20 - dy));
                vertices.push(g.Point(tx, ty - 20 - dy));
                //  console.log("to back right (close)");
            } else {
                vertices.push(g.Point(sx, Math.max(ty + 100, sy + 20) - dy));
                vertices.push(g.Point(tx + 40, Math.max(ty + 100, sy + 20) - dy));
                vertices.push(g.Point(tx + 40, ty - 20 - dy));
                vertices.push(g.Point(tx, ty - 20 - dy));
                //  console.log("to back left (close)");
            }
        }
    }

    // // insert custom vertices between computed ones
    // let newVertices = vertices.slice();
    // let closestDistance = Number.MAX_SAFE_INTEGER;
    // let closestIndex = 1;
    // customVertices.forEach((cv)=>{
    //      for (let i = 0; i < newVertices.length-1; i++){
    //          let mdx = (newVertices[i].x + newVertices[i+1].x)/2
    //          let mdy = (newVertices[i].y + newVertices[i+1].y)/2
    //          let md = {
    //              x: mdx,
    //              y: mdy
    //          }
    //          let currentDistance = Math.sqrt((cv.x - md.x)*(cv.x - md.x) + (cv.y - md.y)*(cv.y - md.y));
    //          if (currentDistance < closestDistance && 
    //             (cv.x > newVertices[i].x && cv.x < newVertices[i+1].x ||
    //              cv.x < newVertices[i].x && cv.x > newVertices[i+1].x ) ||
    //              (cv.y > newVertices[i].y && cv.y < newVertices[i+1].y ||
    //               cv.y < newVertices[i].y && cv.y > newVertices[i+1].y )){
    //             closestDistance = currentDistance;
    //             closestIndex = i+1;
    //          }
    //      }
    //      cv.custom = true;
    //      newVertices.splice(closestIndex, 0, cv);
    // });

    // // adjust computed vertices
    // for (let i = 1; i < newVertices.length - 1; i++){
    //     if (newVertices[i].custom){
    //         if (!newVertices[i-1].custom)
    //             newVertices[i-1].y = newVertices[i].y;
    //         if (!newVertices[i+1].custom)
    //             newVertices[i+1].y = newVertices[i].y;
    //     }
    // }
    return vertices;
}

var graph = new joint.dia.Graph();
var paper = new joint.dia.Paper({
    el: document.getElementById('paper-container-zoomable'),
    model: graph,
    width: canvasWidth,
    height: canvasHeight,
    gridSize: 1,
    embeddingMode: true,
    linkPinning: false,
    validateEmbedding: function (childView, parentView) {
        let result = false;
        if ((parentView.model instanceof Floor || parentView.model instanceof Popup) &&
            !(childView.model instanceof Popup) &&
            parentView.model.attributes.size.width > childView.model.attributes.size.width &&
            parentView.model.attributes.size.height > childView.model.attributes.size.height
        )
            result = true;

        if (parentView.model instanceof SvgImage2 || childView.model instanceof SvgImage2) {
            
            // childView.options.interactive = false;
            // childView.model.attributes.locked;
            console.log(childView, parentView);

            result = true;
        }

        return result;
    },
    drawGrid: {
        name: 'doubleMesh',
        args: [
            { color: 'grey', thickness: 0.2 },                  // settings for the primary mesh
            { color: 'grey', scaleFactor: 5, thickness: 0.3 }   //settings for the secondary mesh
        ]
    },
    interactive: function (cellView) {
        if (cellView.model instanceof joint.dia.Link) {
            // Disable the default vertex add functionality on pointerdown.
            return { vertexAdd: false };
        }
        return true;
    }
});

var commandManager = new joint.dia.CommandManager({
    graph: graph
});

$(document).on('keydown', function (event) {
    //event.preventDefault();
    pressedKey = event.keyCode;

    if (event.ctrlKey == true && (event.which == '61'
        || event.which == '107'
        || event.which == '173'
        || event.which == '109'
        || event.which == '187'
        || event.which == '189')) {
        event.preventDefault();
    }
});

$(document).on('keyup', function (evt) {
    pressedKey = undefined;
    if (evt.ctrlKey && evt.key == 'z') {
        parent.commandManager.undo();
    } else if (evt.ctrlKey && evt.key == 'r') {
        parent.commandManager.redo();
    } else if (evt.ctrlKey && evt.key == 'a') {
        selectionView.destroyAllSelectionBoxes();
        selection.reset();
        paper.model.getCells().forEach((el) => {
            selection.add(el);
            selectionView.createSelectionBox(paper.findViewByModel(el));
        });
    } else if (evt.ctrlKey && evt.key == 'c') {
        clipboard.copyElements(selection, paper.model);
    } else if (evt.ctrlKey && evt.key == 'v') {
        clipboard.pasteCells(paper.model);
        addEventsToNewElements();
        selection.reset();
    } else if (evt.ctrlKey && evt.key == 'x') {
        clipboard.copyElements(selection, paper.model);
        selectionView.model.models.forEach((el) => {
            el.remove();
        });
        selection.reset();
    } else if (evt.ctrlKey && (evt.key == '+' || evt.key == '=')) {
        $panzoom.panzoom('zoom');
    } else if (evt.ctrlKey && evt.key == '-') {
        $panzoom.panzoom('zoom', true);
    } else if (evt.altKey && evt.key == '0') {
        $panzoom.panzoom("reset"); // does not work
    } else if (evt.key == 'Delete') {
        selectionView.model.models.forEach((el) => {
            el.remove();
        });
        selection.reset();
    } else return;
});

graph.on('add', function (cell, collection, opt) {
    // The stencil adds the `stencil` property to the option object with value
    if (opt.stencil) {
        addEventsToNewElements();
        if (!(cell instanceof TextLabel)) {
            // Add unique Export-IDs to elements
            cell.setExportID(ExportIDList.NewID());
        }
        $(paper.findViewByModel(cell).el).trigger('dblclick');
        let intersected = [];
        // embed on adding from stencil
        graph.getElements().forEach(function (el) {
            if ((el instanceof Floor || el instanceof Popup) &&
                !(cell instanceof Popup) &&
                cell !== el &&
                cell.getBBox().intersect(el.getBBox())) {
                intersected.push(el);
            }
        });
        if (intersected.length > 0) {
            intersected[intersected.length - 1].embed(cell);
        }
    }
});

paper.on({
    'element:pointerdown': function (cellView, evt) {
        removePopup();
        if (evt.ctrlKey) {
            snaplines.stopListening();
            paper.findViewByModel(cellView.model).options.interactive = false;
            cellView.update();
            startCloning = true;
        }
        // if (pressedKey == 32){ // space+mousemove
        if (evt.shiftKey && !cellView.model.attributes.nonlinkable) {
            snaplines.stopListening();
            paper.findViewByModel(cellView.model).options.interactive = false;
            cellView.update();
            startLinking = true;
        }
        if (!paper.findViewByModel(cellView.model).options.interactive) {
            snaplines.stopListening();
        }
        startX = evt.clientX;
        startY = evt.clientY;
    },
    'element:pointermove': function (cellView, evt, ppx, ppy) {
        if ((Math.sqrt(Math.pow(Math.abs(startX - evt.clientX), 2) + Math.pow(Math.abs(startY - evt.clientY), 2))) > 20) {
            selection.remove(cellView.model);
            if (startCloning) {
                startCloning = false;
                cloningModel = cellView.model.clone();
                cloningModel.setExportID(ExportIDList.NewID());
                graph.addCell(cloningModel);
                cloningModel.toFront();
                cloningModel.set('z', cellView.model.get('z') + 1);
                addEventsToNewElements();
            }
            if (cloningModel) {
                cloningModel.position(ppx - 20, ppy - 20);
            }
            if (startLinking) {
                startLinking = false;
                newLink = new Link();
                newLink.source(cellView.model);
                newLink.addTo(graph);
            }
            if (newLink) {
                newLink.target({
                    x: ppx,
                    y: ppy
                });
                let viewsUnderTheCursor = paper.findViewsFromPoint({ x: ppx, y: ppy });
                if (viewsUnderTheCursor && viewsUnderTheCursor.length > 0) {
                    if (!viewsUnderTheCursor[viewsUnderTheCursor.length - 1].model.attributes.nonlinkable)
                        viewsUnderTheCursor[viewsUnderTheCursor.length - 1].highlight();
                } else {
                    graph.getElements().forEach(function (el) {
                        paper.findViewByModel(el).unhighlight();
                    });
                }
            }
        }
    },
    'element:pointerup': function (cellView, evt, ppx, ppy) {

        if (joint.ui.TextEditor.ed && joint.ui.TextEditor.ed.options._textContent == '')
            joint.ui.TextEditor.ed.options.cellView.model.attr('text/text', noTextValue);
        joint.ui.TextEditor.close();

        if (!cellView.model.attributes.locked) {
            paper.findViewByModel(cellView.model).options.interactive = true;
            cellView.update();
        }
        startCloning = false;
        startX = undefined;
        startY = undefined;
        if (evt.ctrlKey && !cloningModel) {
            selection.add(cellView.model);
            selectionView.createSelectionBox(paper.findViewByModel(cellView.model));
        }
        cloningModel = undefined;

        if (evt.ctrlKey || cellView.model instanceof joint.shapes.devs.Link) {
            return;
        }
        var halo = new joint.ui.Halo({ cellView: cellView, useModelGeometry: false, type: 'toolbar', boxContent: false });
        if (!(cellView.model instanceof TextLabel)) {
            halo.removeHandle("rotate");
        }
        halo.removeHandle("boxContent");
        halo.removeHandle("resize");
        halo.removeHandle("clone");
        halo.removeHandle("fork");
        halo.removeHandle("link");
        halo.addHandle({ name: 'bringtofront', position: 's', icon: 'icons/send-to-front.svg' });
        halo.on('action:bringtofront:pointerdown', function (evt) {
            evt.stopPropagation();
            cellView.model.toFront({ deep: true });
        });
        halo.addHandle({ name: 'sendtoback', position: 's', icon: 'icons/send-to-back.svg' });
        halo.on('action:sendtoback:pointerdown', function (evt) {
            evt.stopPropagation();
            cellView.model.toBack({ deep: true });
        });

        if (!(cellView.model instanceof TextLabel) && !(cellView.model instanceof Floor)) {
            halo.addHandle({ name: 'reflect', position: 's', icon: 'icons/reflect.svg' });
            halo.on('action:reflect:pointerdown', function (evt) {
                evt.stopPropagation();
                cellView.model.attributes.reflected = !cellView.model.attributes.reflected;
                let texttransformation;
                const transformation = cellView.model.attributes.reflected ?
                    'scale(-1,1.15470774347)skewX(30)rotate(-30)translate(-34px, 0px)' :
                    'scale(1,1.15470774347)skewX(-30)rotate(30)';
                if (!(cellView.model instanceof SvgImage2)) {
                    texttransformation = cellView.model.attributes.reflected ?
                        'translate(-23, 20)rotate(90)' : 'translate(5, 50)';
                } else {
                    texttransformation = cellView.model.attributes.reflected ?
                        'translate(-16, 0)skewX(-45)skewY(45)' : 'translate(26, 41)skewX(-45)';
                    cellView.model.attributes.attrs.label.textAnchor =
                        cellView.model.attributes.reflected ? "end" : "start";
                }

                cellView.model.attributes.attrs.icon.transform = transformation;
                cellView.model.attributes.attrs.label.transform = texttransformation;
                cellView.update();
            });
        }

        let lockIcon = cellView.model.attributes.locked ? 'icons/lock.svg' : 'icons/unlock.svg';
        halo.addHandle({ name: 'lock', icon: lockIcon });
        halo.on('action:lock:pointerdown', function (evt) {
            evt.stopPropagation();
            cellView.model.attributes.locked = !cellView.model.attributes.locked;
            $(paper.findViewByModel(cellView.model).el).toggleClass('locked', null);
            paper.findViewByModel(cellView.model).options.interactive = !cellView.model.attributes.locked;
            cellView.update();
            lockIcon = cellView.model.attributes.locked ? 'icons/lock.svg' : 'icons/unlock.svg';
            $($(".joint-halo").find('.handle.lock')[0]).attr('style', 'background-image: url("' + lockIcon + '");');
        });

        var parentelement;
        graph.getCells().forEach(element => {
            if (cellView.model.isEmbeddedIn(element)) {
                parentelement = element;
            }
        });
        if (parentelement) {
            halo.addHandle({ name: 'unembed', position: 's', icon: 'icons/unembed.svg' });
            halo.on('action:unembed:pointerdown', function (evt) {
                evt.stopPropagation();
                parentelement.unembed(cellView.model);
                halo.removeHandle("unembed");
            });
        }

        halo.render();
        if (!(cellView.model instanceof Service) &&
            !(cellView.model instanceof SvgImage) &&
            !(cellView.model instanceof SvgImage2) &&
            !(cellView.model instanceof TextLabel)) {
            var freeTransform = new joint.ui.FreeTransform({ cellView: cellView, allowRotation: false });
            freeTransform.render();
        }
        if (!cellView.model.attributes.locked) {
            paper.findViewByModel(cellView.model).options.interactive = true;
            cellView.update();
        }

        var viewsUnderTheCursor = paper.findViewsFromPoint({ x: ppx, y: ppy });
        if (newLink) {
            if (viewsUnderTheCursor[viewsUnderTheCursor.length - 1] && !viewsUnderTheCursor[viewsUnderTheCursor.length - 1].model.attributes.nonlinkable) {
                newLink.target(viewsUnderTheCursor[viewsUnderTheCursor.length - 1].model);

                adjustLinkTargets(newLink.getTargetElement());
                adjustLinkSources(newLink.getSourceElement());

                newLink.on({
                    'change': function (e) {
                        adjustLinkTargets(e.attributes.target);
                        adjustLinkSources(e.attributes.source);
                    }
                });
            } else {
                newLink.remove();
            }
            graph.getElements().forEach(function (el) {
                paper.findViewByModel(el).unhighlight();
            });
        }

        if ($('#chk-showSnaplines')[0].checked) {
            snaplines.startListening();
        }
        newLink = undefined;

        // send Floor to back if it does not have embedded elements
        if (cellView.model instanceof Floor && cellView.model.getEmbeddedCells().length == 0) {
            $.each(graph.findModelsUnderElement(cellView), (i, el) => {
                if (el instanceof Floor && el != cellView.model) {
                    cellView.model.toFront();
                    return false;
                } else {
                    cellView.model.toBack();
                }
            });
        }

        // keep popups on top of other elements
        graph.getElements().forEach(function (model) {
            if (model instanceof Popup) {
                model.toFront({ deep: true });
            }
        });

        joint.ui.TextEditor.edit(evt.target, {
            cellView: cellView,
            textProperty: 'attrs/text/text',
            // textareaAttributes: null,
            textareaAttributes: {
                autocorrect: 'off',
                //  autocomplete: null,
                autocapitalize: 'off',
                spellcheck: 'off',
                tabindex: 0
            },
            // placeholder: true
        });
    },
    'element:contextmenu': function (cellView, evt) {
        $(cellView.el).trigger('dblclick');
    },
    'blank:pointerdown': function (evt, ppx, ppy) {
        selection.reset();
        selectionView.destroyAllSelectionBoxes();
        ppx0 = ppx;
        ppy0 = ppy;
        $('.joint-selection').css({
            'display': 'block',
            'left': 0,
            'top': 0,
            'transform-origin': 'top left 0',
        })
    },
    'blank:pointermove': function (evt, ppx, ppy) {
        $('.joint-selection').css({
            'transform': 'rotate(-30deg)skewX(30deg)scale(1, 0.86602)translate(' + (ppx - ppx0 < 0 ? ppx : ppx0) + 'px,' + (ppy - ppy0 < 0 ? ppy : ppy0) + 'px)',
            'width': Math.abs(ppx0 - ppx),
            'height': Math.abs(ppy0 - ppy),
        });
    },
    'blank:pointerup': function (evt, ppx, ppy) {
        $('.joint-selection').removeAttr('style');
        var selectList = paper.findViewsInArea({
            x: Math.min(ppx, ppx0),
            y: Math.min(ppy, ppy0),
            width: Math.abs(ppx0 - ppx),
            height: Math.abs(ppy0 - ppy)
        });
        selectList.forEach(element => {
            selection.add(element.model);
            selectionView.createSelectionBox(element);
        });

        if (joint.ui.TextEditor.ed && joint.ui.TextEditor.ed.options._textContent == '')
            joint.ui.TextEditor.ed.options.cellView.model.attr('text/text', noTextValue);
        joint.ui.TextEditor.close();
    },
});

// add [[no text]] for empty texts
joint.ui.TextEditor.on('caret:change', function () {
    if (joint.ui.TextEditor.ed._textareaValueBeforeInput == noTextValue) {
        joint.ui.TextEditor.ed.options.cellView.model.attr('text/text', '');
    } else if (joint.ui.TextEditor.ed._textareaValueBeforeInput == '') {
        joint.ui.TextEditor.ed.options.cellView.model.attr('text/text', noTextValue);
    }
});

function adjustLinkTargets(model) {
    let links = graph.getConnectedLinks(model, { inbound: true });
    let numberOfLinks = links.length;
    let step = 5;
    
    links.sort(function (a, b) {
        let x1 = paper.findViewByModel(a).sourceBBox ? paper.findViewByModel(a).sourceBBox.center().x : 0;
        let x2 = paper.findViewByModel(a).sourceBBox ? paper.findViewByModel(b).sourceBBox.center().x : 0;
        let y1 = paper.findViewByModel(a).sourceBBox ? paper.findViewByModel(a).sourceBBox.center().y : 0;
        let y2 = paper.findViewByModel(a).sourceBBox ? paper.findViewByModel(b).sourceBBox.center().y : 0;
        let x0 = paper.findViewByModel(a).targetBBox ? paper.findViewByModel(a).targetBBox.center().x : 0;
        let y0 = paper.findViewByModel(a).targetBBox ? paper.findViewByModel(a).targetBBox.center().y : 0;

        let a1 = Math.atan(-(y0-y2)/(x0-x2));
        let a2 = Math.atan(-(y0-y1)/(x0-x1));

        if (a1>a2) return -1
        else if (a1<a2) return 1
        else return 0;
    });

    links.forEach((el, i) => {
        el.attributes.target.anchor = {
            name: 'top',
            args: {
                dx: (i + 0.5 - numberOfLinks / 2) * step,
                dy: 0
            }
        }
        el.attributes.target.connectionPoint = {
            name: 'bbox',
            args: {
                offset: -20
            }
        }

        //set links under objects
        let linkZ = el.get('z');
        if (!startLinking) {
            el.getTargetElement().set('z', linkZ + 1);
        }
        paper.findViewByModel(el).update();
    });
}

function adjustLinkSources(model) {
    let links = graph.getConnectedLinks(model, { outbound: true });
    let numberOfLinks = links.length;
    let step = 5;

    links.sort(function (a, b) {
        let x1 = paper.findViewByModel(a).targetBBox ? paper.findViewByModel(a).targetBBox.center().x : 0;
        let x2 = paper.findViewByModel(a).targetBBox ? paper.findViewByModel(b).targetBBox.center().x : 0;
        let y1 = paper.findViewByModel(a).targetBBox ? paper.findViewByModel(a).targetBBox.center().y : 0;
        let y2 = paper.findViewByModel(a).targetBBox ? paper.findViewByModel(b).targetBBox.center().y : 0;
        let x0 = paper.findViewByModel(a).sourceBBox ? paper.findViewByModel(a).sourceBBox.center().x : 0;
        let y0 = paper.findViewByModel(a).sourceBBox ? paper.findViewByModel(a).sourceBBox.center().y : 0;

        let a1 = Math.atan((x0-x2)/(y0-y2));
        let a2 = Math.atan((x0-x1)/(y0-y1));
                
        if (a1>a2) return 1
        else if (a1<a2) return -1

        return 0;
    });

    links.forEach((el, i) => {
        el.attributes.source.anchor = {
            name: 'bottom',
            args: {
                dx: (i + 0.5 - numberOfLinks / 2) * step,
                dy: 0
            }
        }
        el.attributes.source.connectionPoint = {
            name: 'bbox'
        }

        //set links under objects
        let linkZ = el.get('z');
        el.getSourceElement().set('z', linkZ + 1);

        paper.findViewByModel(el).update();
    })
}

var stencil = new joint.ui.Stencil({
    graph: graph,
    paper: paper
});

$('#stencil-container').mouseleave(function (event) {
    if (stencil._clone) {
        dropping = true;
    }
});

// SVG filter to imitate layer thickness
var thicknessFilterId = paper.defineFilter({
    name: 'dropShadow',
    args: {
        dx: -4,
        dy: 4,
        blur: 0,
        color: "#dddddd",
    }
});

//fix bbox size for objectw with filters
$("#" + thicknessFilterId)
    .attr('x', -.1)
    .attr('y', 0)
    .attr('width', 1.1)
    .attr('height', 1.1);

// SVG filter for shadow
var shadowFilterId = paper.defineFilter({
    name: 'dropShadow',
    args: {
        dx: -20,
        dy: 20,
        blur: 10,
        opacity: 0.2,
        color: "#000000",
    }
});

//fix bbox size
$("#" + shadowFilterId)
    .attr('x', -.3)
    .attr('y', 0)
    .attr('width', 1.3)
    .attr('height', 1.3);

// snaplines
var snaplines = new joint.ui.Snaplines({ paper: paper });

var cylinderLinearGradientId = paper.defineGradient({
    type: 'linearGradient',
    stops: [
        { offset: '0%', color: '#A0A6A8' },
        { offset: '25.5%', color: '#C4CBD0' },
        { offset: '42%', color: '#C8D0D4' },
        { offset: '64.5%', color: '#D8DDE1' },
        { offset: '70%', color: '#DDE1E5' },
        { offset: '90%', color: '#DAE0E3' },
        { offset: '100%', color: '#D6DDE0' }
    ],
    attrs: {
        x1: '0%',
        y1: '50%',
        x2: '100%',
        y2: '50%'
    }
});

var cylinderShadowGradientId = paper.defineGradient({
    type: 'radialGradient',
    stops: [
        { offset: '0%', color: '#010202' },
        { offset: '85%', color: '#010202', opacity: 0.14 },
        { offset: '100%', color: '#010202', opacity: 0 }
    ],
    attrs: {
        x1: '0%',
        y1: '50%',
        x2: '100%',
        y2: '50%'
    }
});

// define a text Label
var TextLabel = joint.dia.Element.define('TextLabel', {
    nonlinkable: true,
    allowPopup: false,
    attrs: {
        '.': { magnet: false },
        text: {
            refX: '15px',
            refY: '50%',
            yAlignment: 'middle',
            xAlignment: 'middle',
            class: 'font_family',
            text: 'text',
            'text-anchor': 'middle',
            fontSize: 20
        }
    }
}, {
        markup: [{
            tagName: 'text',
            selector: 'text'
        }],
        setText: function (text) {
            return this.attr('text/text', text || '');
        },
        setColor: function (hexColor) {
            this.attr('text/fill', hexColor);
        },
        setSize: function (fontSize) {
            if (fontSize > 5) {
                this.attr('text/fontSize', fontSize);
            }
        },
        getSize: function () {
            return this.attr('text/fontSize');
        },
        setExportID: function () {
            // do nothing
        }
    });

// define an image shape
var SvgImage = joint.dia.Element.define('SvgImage', {
    size: {
        width: 50,
        height: 50
    },
    'export-id': 'service-' + undefined,
    allowPopup: true,
    attrs: {
        icon: {
            refWidth: '80%',
            refHeight: '80%',
            refX: '50%',
            refY: '-40%',
            'stroke-width': 0.5,
            class: 'hl-icon',
            'href': 'icons/firewall.svg',
            transform: 'scale(1,1.15470774347)skewX(-30)rotate(30)',
            'icon': undefined,
        },
        label: {
            textAnchor: 'middle',
            transform: 'translate(5, 50)',
            //refX: '20%',
            //refY: '-20%',
            y: -5,
            fontSize: 12,
            fill: '#333333',
            class: 'font_family',
        }
    }
}, {
        markup: [{
            tagName: 'image',
            selector: 'icon',
        }, {
            tagName: 'text',
            selector: 'label'
        }],
        setText: function (text) {
            return this.attr('text/text', text || '');
        },
        setExportID: function (id) {
            this.attributes['export-id'] = 'service-' + id;
        },
        getExportID: function () {
            return this.attributes['export-id'].substr(8);
        },
        setIcon: function (iconName) {
            this.attr('icon/icon', iconName);
            this.attr('icon/href', iconName);
        }
    });

// define an image shape
var SvgImage2 = joint.dia.Element.define('SvgImage2', {
    size: {
        width: 50,
        height: 50
    },
    'export-id': 'service-' + undefined,
    allowPopup: true,
    attrs: {
        icon: {
            refWidth: '100%',
            refHeight: '100%',
            refX: '50%',
            refY: '-40%',
            'stroke-width': 0.5,
            class: 'hl-icon',
            'href': 'icons/switch.svg',
            transform: 'scale(1,1.15470774347)skewX(-30)rotate(30)',
            'icon': undefined,
        },
        label: {
            textAnchor: 'start',
            transform: 'translate(26, 41)skewX(-45)',
            y: -5,
            fontSize: 8,
            fill: '#333333',
            class: 'font_family',
        }
    }
}, {
        markup: [{
            tagName: 'image',
            selector: 'icon',
        }, {
            tagName: 'text',
            selector: 'label'
        }],
        setText: function (text) {
            return this.attr('text/text', text || '');
        },
        setExportID: function (id) {
            this.attributes['export-id'] = 'service-' + id;
        },
        getExportID: function () {
            return this.attributes['export-id'].substr(8);
        },
        setIcon: function (iconName) {
            this.attr('icon/icon', iconName);
            this.attr('icon/href', iconName);
        }
    });

// define a floor shape
var Floor = joint.dia.Element.define('Floor', {
    'export-id': 'service-' + undefined,
    nonlinkable: true,
    allowPopup: false,
    attrs: {
        rect: {
            refWidth: '100%',
            refHeight: '100%',
            style: 'fill:#fafafa',
            rx: 15,
            ry: 15,
            filter: 'url(#' + thicknessFilterId + ')',
        },
        recthl: {
            refWidth: '100%',
            refHeight: '100%',
            class: 'invisible',
            style: 'opacity:0.2',
            rx: 15,
            ry: 15,
            'export-id': 'hl-' + undefined,
        },
        text: {
            refX: '15px',
            refY: '50%',
            yAlignment: 'middle',
            xAlignment: 'middle',
            class: 'font_family',
            transform: 'rotate(90)',
            'text-anchor': 'middle',
            fontSize: 18,
        },
        '.': { magnet: false }
    }
}, {
        markup: [{
            tagName: 'rect',
            selector: 'rect',
        }, {
            tagName: 'rect',
            selector: 'recthl',
        }, {
            tagName: 'text',
            selector: 'text',
        }],
        setText: function (text) {
            return this.attr('text/text', text || '');
        },
        setExportID: function (id) {
            this.attr('recthl/export-id', 'hl-' + id);
            this.attributes['export-id'] = 'service-' + id;
        },
        getExportID: function () {
            return this.attributes['export-id'].substr(8);
        },
        getColor: function () {
            return this.attr('rect/style').replace(/fill:/, '');
        },
        setColor: function (hexColor) {
            this.attr('rect/style', 'fill:' + hexColor);
        }
    });

// define a popup shape
var Popup = joint.dia.Element.define('Popup', {
    'export-id': 'service-' + undefined,
    nonlinkable: true,
    attrs: {
        rect: {
            refWidth: '100%',
            refHeight: '100%',
            style: 'fill:#fafafa;stroke:#cccccc;stroke-width:0.25px;',
            rx: 15,
            ry: 15,
            filter: 'url(#' + thicknessFilterId + ')',
        },
        shadow: {
            refWidth: '100%',
            refHeight: '100%',
            style: 'fill:#ffffff',
            rx: 15,
            ry: 15,
            filter: 'url(#' + shadowFilterId + ')',
        },
        recthl: {
            refWidth: '100%',
            refHeight: '100%',
            class: 'invisible',
            style: 'opacity:0.2',
            rx: 15,
            ry: 15,
            'export-id': 'hl-' + undefined,
        },
        text: {
            refX: '15px',
            refY: '50%',
            yAlignment: 'middle',
            xAlignment: 'middle',
            class: 'font_family',
            transform: 'rotate(90)',
            'text-anchor': 'middle',
            fontSize: 18,
        },
        '.': { magnet: false }
    }
}, {
        markup: [{
            tagName: 'rect',
            selector: 'shadow',
        }, {
            tagName: 'rect',
            selector: 'rect',
        }, {
            tagName: 'rect',
            selector: 'recthl',
        }, {
            tagName: 'text',
            selector: 'text',
        }],
        setText: function (text) {
            return this.attr('text/text', text || '');
        },
        setExportID: function (id) {
            this.attr('recthl/export-id', 'hl-' + id);
            this.attributes['export-id'] = 'service-' + id;
        },
        getExportID: function () {
            return this.attributes['export-id'].substr(8);
        },
        getColor: function () {
            return this.attr('rect/style').replace(/fill:/, '');
        },
        setColor: function (hexColor) {
            this.attr('rect/style', 'fill:' + hexColor);
        }
    });

// define a service
var Service = joint.dia.Element.define('Service', {
    size: {
        width: 50,
        height: 50
    },
    'export-id': 'service-' + undefined,
    allowPopup: true,
    attrs: {
        highlight: {
            refRx: '60%',
            refRy: '34.641%',
            refCx: '50%',
            refCy: '15%',
            class: 'invisible',
            transform: 'scale(1,1.15470774347)skewX(-30)rotate(30)',
            'export-id': "hl-" + undefined,
        },
        side: {
            refWidth: '100%',
            refHeight: '12%',
            fill: 'url(#' + cylinderLinearGradientId + ')',
            transform: 'scale(1,1.15470774347)skewX(-30)rotate(30)',
        },
        bottom: {
            refRx: '50%',
            refRy: '28.867513459%',
            refCx: '50%',
            refCy: '12%',
            fill: 'url(#' + cylinderLinearGradientId + ')',
            transform: 'scale(1,1.15470774347)skewX(-30)rotate(30)',
        },
        top: {
            refRx: '50%',
            refRy: '28.867513459%',
            refCx: '50%',
            class: 'cylinder-top',
            transform: 'scale(1,1.15470774347)skewX(-30)rotate(30)',
        },
        icon: {
            refWidth: '70%',
            refHeight: '70%',
            refX: '58%',
            refY: '-40%',
            'stroke-width': 0.5,
            'href': 'icons/box.svg',
            class: 'service-icon',
            transform: 'scale(1,1.15470774347)skewX(-30)rotate(30)', // 'scale(-1,1.15470774347)skewX(30)rotate(-30)translate(-34px, 0px)' //scale(-1,1)translate(-34px, 0px)
            'icon': undefined,
        },
        label: {
            textAnchor: 'middle',
            fontSize: 12,
            fill: '#333333',
            class: 'font_family',
            transform: 'translate(10, 48)',
        }
    }
}, {
        markup: [{
            tagName: 'ellipse',
            selector: 'highlight',
        }, {
            tagName: 'rect',
            selector: 'side',
        }, {
            tagName: 'ellipse',
            selector: 'bottom',
        }, {
            tagName: 'ellipse',
            selector: 'top',
        }, {
            tagName: 'image',
            selector: 'icon',
        }, {
            tagName: 'text',
            selector: 'label'
        }],
        setText: function (text) {
            return this.attr('text/text', text || '');
        },
        setExportID: function (id) {
            this.attr('highlight/export-id', 'hl-' + id);
            this.attributes['export-id'] = 'service-' + id;
        },
        getExportID: function () {
            return this.attributes['export-id'].substr(8);
        },
        setIcon: function (iconName) {
            this.attr('icon/icon', iconName);
            this.attr('icon/href', iconName);
        }
    });


function removePopup() {
    $('.joint-popup').remove();
}

function showHintDialog() {
    // jquery-ui dialog
    $(function () {
        $(".wrapper").append(`<div id="hintDialog"><p style="font-family: din-lightregular;font-size:11px;">
        <b>[shift] + drag</b> on an element - create a link<br>
        <b>[ctrl] + drag</b> - clone an element<br>
        <b>double click</b> or <b>right mouse button</b> on an element - show the element settings<br>
        <b>[ctrl] + left mouse button</b> on an element - select (multiple selection)<br></div>`);

        $("#hintDialog").dialog({
            title: "MAIN CONTROLS",
            autoOpen: false,
            resizable: false,
            height: 240,
            width: 170,
            position: { my: "left+8 top+55", at: "left+8 top+55", of: window },
            create: function (event, ui) {
                var widget = $(this).dialog("widget");
                $(".ui-dialog-titlebar-close span", widget)
                    .removeClass("ui-icon-closethick")
                    .addClass("ui-icon-close");
            }
        });
        $("#hintDialog").dialog("open");
    });
}

let embeddedSVGScript = `// highlight links for a selected element
(${hoverEffectScript})();
// demo message
console.log("Generated in SVG editor");`;

function svgScript() {

    let h = $(window).height() * 0.7;
    let w = $(window).width() * 0.9;

    $(function () {
        $(".wrapper").append(`<div id="svgScriptDialog">
        <div id="js-svg-preview" style="width:${w - 5}px;height:${h - 95}px;max-height:100%;max-width:100%;"></div>
        </div>`);
        $("#svgScriptDialog").dialog({
            title: () => "Embedded JavaScript".toUpperCase(),
            //resizable: false,
            autoOpen: false,
            height: h,
            width: w,
            maxHeight: h,
            maxWidth: w,
            buttons: [
                {
                    text: "SAVE",
                    // icon: "ui-icon-disk",
                    click: function () {
                        embeddedSVGScript = jsEditorValue;
                        $(this).dialog("close");
                    }
                }
            ],
            create: function (event, ui) {
                var widget = $(this).dialog("widget");
                $(".ui-dialog-titlebar-close span", widget)
                    .removeClass("ui-icon-closethick")
                    .addClass("ui-icon-close");
            }
        });
        initJSCodeEditor("js-svg-preview", embeddedSVGScript);
        $("#svgScriptDialog").dialog("open");
    });
}

function newProject() {
    $(function () {
        $(".wrapper").append(`<div id="svgNewProjectDialog"><em class="icon-question" style="
        font-size: 61px;
        position: absolute;
        top: 45px;
        left: 41px;
        color: #d3d3d3;
    "></em><div style="
        text-align: right;
        position: absolute;
        left: 142px;
        top: 28px;"><h3>Create a new project?<br></h3><p>All unsaved changes will be lost</p></div></div>`);

        $("#svgNewProjectDialog").dialog({
            title: () => "Create a new project?".toUpperCase(),
            resizable: false,
            draggable: false,
            autoOpen: true,
            modal: true,
            height: 200,
            width: 400,
            buttons: [
                {
                    text: "YES",
                    click: function () {
                        graph.clear();
                        $(this).dialog("close");
                    }
                },
                {
                    text: "CANCEL",
                    click: function () {
                        $(this).dialog("close");
                    }
                }
            ],
            create: function (event, ui) {
                var widget = $(this).dialog("widget");
                // $(".ui-dialog-titlebar-close span", widget)
                //     .removeClass("ui-icon-closethick")
                //    .addClass("ui-icon-close");
                $(".ui-dialog-titlebar", widget).remove();
            }
        });
    });
}

function saveProjectToJSON() {
    var fileExists;
    currentFileName = selectedProjectFile === undefined ? 'new diagram - [' + (Date.parse(new Date())) + ']' : selectedProjectFile.replace(/\.dia\.json$|\.dia$|\.json$/i, '');
    var jsonObject = graph.toJSON();

    $(function () {
        $(".wrapper").append(`<div id="jsonSaveDialog">
        <h5 style="margin: 0;">File Name:</h5>
        <input id="newFileName" style="width: 100%;" value="${currentFileName}" />
        <div id="fileList">No projects found</div>
        </div>`);
        $("#jsonSaveDialog").dialog({
            title: () => "Save the project".toUpperCase(),
            //resizable: false,
            autoOpen: false,
            height: 600,
            width: 800,
            buttons: [
                {
                    text: "SAVE",
                    click: function () {
                        saveFile();
                        $(this).dialog("close");
                    }
                },
                {
                    text: "CLOSE",
                    click: function () {
                        $(this).dialog("close");
                    }
                }
            ],
            create: function (event, ui) {
                var widget = $(this).dialog("widget");
                $(".ui-dialog-titlebar-close span", widget)
                    .removeClass("ui-icon-closethick")
                    .addClass("ui-icon-close");

                widget.find("#fileList").fileTree({ root: "../../../dashboard/images/svgeditorexport/projects/", script: "includes/jqueryfiletree/jqueryFileTree.php" }, function (file) {
                    if (file.slice(-9) === ".dia.json") {
                        fileExists = true;
                        selectedProjectFile = file.slice(17).replace(/\.dia\.json$|\.dia$|\.json$/i, '');
                        $("#newFileName").attr("value", selectedProjectFile);
                    }
                });

                widget.find("#fileList").on('filetreeclicked', function (e, data) {

                });
            }
        });
        $("#jsonSaveDialog").dialog("open");
    });

    function saveFile() {
        if (fileExists) {
            var result = confirm('A file with this name already exists. Do you want to owerwrite it?');
            if (!result) return;
        }
        var data = new FormData();
        data.append('filename', $('#newFileName')[0].value.replace(/\.dia\.json$|\.dia$|\.json$/i, ''));
        data.append('data', JSON.stringify(jsonObject));
        data.append('mime_content_type', 'application/json');
        var xhr = new XMLHttpRequest();
        xhr.open('post', 'includes/jqueryfiletree/manageProjectFiles.php', true);
        xhr.send(data);
    }
}

function openJSONProject() {
    // selectedProjectFile = undefined;
    $(".wrapper").append(`<div id="svgOpenDialog">
        <div id="selectedFile"><p>No selected project...</p></div><div id="fileList">No projects found</div>
            <script>
                $("#fileList").fileTree({ root: "/opt/ozona/obsidian/dashboard/images/svgeditorexport/projects/", script: "includes/jqueryfiletree/jqueryFileTree.php" }, function(file) {
                    if (file.slice(-9)===".dia.json"){
                        selectedProjectFile = file;
                        $("#selectedFile").html("<p>"+file+"</p>");}
            		});
            </script>
   </div>`);
    $("#svgOpenDialog").dialog({
        title: () => "Open the project".toUpperCase(),
        //resizable: false,
        autoOpen: false,
        height: 600,
        width: 800,
        buttons: [
            {
                text: "OPEN",
                click: function () {
                    selectFile();
                    $(this).dialog("close");
                }
            },
            {
                text: "CLOSE",
                click: function () {
                    $(this).dialog("close");
                }
            }
        ],
        create: function (event, ui) {
            var widget = $(this).dialog("widget");
            $(".ui-dialog-titlebar-close span", widget)
                .removeClass("ui-icon-closethick")
                .addClass("ui-icon-close");

            widget.find("#fileList").on('filetreeclicked', function (e, data) {

            });
        }
    });
    $("#svgOpenDialog").dialog("open");

    function selectFile() {
        if (selectedProjectFile === undefined) {
            alert('No file has been selected');
        } else {
            // load the file
            $.get(selectedProjectFile.replace(/^\/opt\/ozona/,''), function (result) {
                // open file in the workspace
                graph.fromJSON(result);
                addEventsToNewElements();
                // define the max id value in opened document to prevent duplicate IDs for new elements
                let previousIDnum = 1;
                graph.attributes.cells.models.forEach((el) => {
                    if (el.attributes["export-id"]) {
                        const currentIDnum = el.attributes["export-id"].replace(/^service-/, '')
                        if (!isNaN(currentIDnum)) {
                            previousIDnum = Math.max(previousIDnum, currentIDnum);
                        }
                    }
                });
                ExportIDList.LastIDNum = previousIDnum;
            });
        }
    }
}

function showTextLabelPopup(e1) {

    var serviceView = e1.currentTarget;
    var cell = graph.getCell($(serviceView).attr('model-id'));
    if (!cell) return;
    serviceView = $(serviceView).attr('model-id') ? serviceView : $($(e1.target).parent()[0]).parent()[0];
    var Popup = (new joint.ui.Popup({
        content: ['<label>Size:</label><input id="Popup-Size" width="200" value="' + cell.getSize() + '" style="float:right; width:100px"></input>',
        ],
        events: {
            'input #Popup-Size': function () {
                cell.setSize($("#Popup-Size")[0].value);
            }
        },
        target: this,
    })).render();
}

function showServicePopup(e1) {
    var serviceView = e1.currentTarget;
    let iconsOptions = e1.data.iconsOptions;
    let cell = graph.getCell($(serviceView).attr('model-id'));
    if (!cell) return;
    if (!iconsOptions || !Array.isArray(iconsOptions)) {
        iconsOptions = [
            { icon: 'icons/box.svg', content: 'Box', selected: true },
            { icon: 'icons/boxes.svg', content: 'Boxes' },
            { icon: 'icons/racks.svg', content: 'Racks' },
            { icon: 'icons/laptop.svg', content: 'Laptop' },
            { icon: 'icons/firewall.svg', content: 'Firewall' },
            { icon: 'icons/isp.svg', content: 'ISP' },
            { icon: 'icons/mpls.svg', content: 'MPLS' },
            { icon: 'icons/router.svg', content: 'Router' },
            { icon: 'icons/switch.svg', content: 'Switch' },
            { icon: 'icons/virtual-server.svg', content: 'Virtual Server' },
            { icon: 'icons/physical-server.svg', content: 'Phyical Server' }
        ]
    }
    var Popup = (new joint.ui.Popup({
        content: [function () {
            var selectBox = new joint.ui.SelectBox({
                width: 150,
                target: this,
                options: iconsOptions
            });
            selectBox.on('option:select', function (e) {
                cell.setIcon(e.icon);
            });
            return selectBox.render().el;
        },
        '<br><br><label>ID:</label><input  id="Popup-Service-ID" value="' + cell.getExportID() + '" style="float:right; width:100px"></input>',
            '<br><br><button id="Apply_ID" style="float:right; width:70px; display:none;">Apply ID</button>'],

        events: {
            'input #Popup-Service-ID': function () {
                var typedID = $("#Popup-Service-ID")[0].value;
                // Check if Export-ID is unique
                if (ExportIDList.ExportIDs.has(typedID) || typedID == '') {
                    $("#Apply_ID").hide();
                } else {
                    $("#Apply_ID").show();
                }
            },
            'click #Apply_ID': function () {
                const target = e1.isTrigger ? e1.target : e1.currentTarget;
                ExportIDList.UpdateID((graph.getCell($(target).attr('model-id'))).getExportID(), $("#Popup-Service-ID")[0].value);
                (graph.getCell($(serviceView).attr('model-id'))).setExportID($("#Popup-Service-ID")[0].value);
                $("#Apply_ID").hide();
            }
        },
        target: this
    })).render();
}

function showFloorPopup(e1) {

    var floorView = e1.currentTarget;
    var cell = graph.getCell($(floorView).attr('model-id'));
    if (!cell) return

    var Popup = (new joint.ui.Popup({
        content: [
            '<br><br><label>ID:</label><input id="Popup-Floor-ID" value="' + cell.getExportID() + '" style="float:right; width:100px"></input>',
            '<br><br><label>Color:</label><select id="Popup-Floor-color"  width="200" style="float:right; width:100px">\
                <option value="#fdfdfd">Light light grey</option>\
                <option value="#fafafa">Light grey (default)</option>\
                <option value="#eeeeee">Grey</option>\
                <option value="#dddddd">Dark grey</option>\
            </select>',
            '<br><br><button id="Apply_ID" style="float:right; width:70px; display:none;">Apply ID</button>'
        ],
        events: {
            'input #Popup-Floor-ID': function () {
                var typedID = $("#Popup-Floor-ID")[0].value;
                // Check if Export-ID is unique
                if (ExportIDList.ExportIDs.has(typedID) || typedID == '') {
                    $("#Apply_ID").hide();
                } else {
                    $("#Apply_ID").show();
                }
            },
            'input #Popup-Floor-color': function () {
                (graph.getCell($(floorView).attr('model-id'))).setColor($("#Popup-Floor-color")[0].value);
            },
            'click #Apply_ID': function () {
                ExportIDList.UpdateID((graph.getCell($(floorView).attr('model-id'))).getExportID(), $("#Popup-Floor-ID")[0].value);
                (graph.getCell($(floorView).attr('model-id'))).setExportID($("#Popup-Floor-ID")[0].value);
                $("#Apply_ID").hide();
            }
        },
        target: this
    })).render();
}

function showPopupPopup(e1) {

    var popupView = e1.currentTarget;
    var cell = graph.getCell($(popupView).attr('model-id'));
    var currentExportID = cell.getExportID();
    var currentParentID = (cell.attributes.parentID ? cell.attributes.parentID : "");
    if (!cell) return

    var Popup = (new joint.ui.Popup({
        content: [
            '<br><br><label>ID:</label><input id="Popup-Popup-ID" value="' + currentExportID + '" style="float:right; width:100px"></input>',
            '<br><br><label>Parent ID:</label><input id="Popup-Popup-Parent-ID" value="' + currentParentID + '" style="float:right; width:100px"></input>',
            '<br><br><label>Color:</label><select id="Popup-Popup-color"  width="200" style="float:right; width:100px">\
                <option value="#fdfdfd">Light light grey</option>\
                <option value="#fafafa">Light grey (default)</option>\
                <option value="#eeeeee">Grey</option>\
                <option value="#dddddd">Dark grey</option>\
            </select>',
            '<br><br><button id="Apply_ID" style="float:right; width:70px; display:none;">Apply ID</button>'
        ],
        events: {
            'input #Popup-Popup-ID,#Popup-Popup-Parent-ID': function () {
                let typedID = $("#Popup-Popup-ID")[0].value;
                let parentID = $("#Popup-Popup-Parent-ID")[0].value;
                let allowPopup = false;
                let embedded = false;

                $.each(cell.getEmbeddedCells(), (i, el) => {
                    if (el.attributes['export-id'].replace(/^service-/g, '') == parentID) {
                        embedded = true;
                        return false;
                    }
                });

                $.each(graph.getElements(), (i, el) => {
                    if (el.attributes['export-id'].replace(/^service-/g, '') == parentID) {
                        allowPopup = el.attributes.allowPopup;
                    }
                });
                // Check if Export-ID is unique and parent for popup can be assigned
                if ((ExportIDList.ExportIDs.has(typedID) && typedID != currentExportID) ||
                    embedded ||
                    typedID == '' ||
                    parentID == '' ||
                    !allowPopup ||
                    (parentID == currentParentID && typedID == currentExportID) ||
                    !(ExportIDList.ExportIDs.has(parentID)) ||
                    parentID == typedID) {
                    $("#Apply_ID").hide();
                } else {
                    $("#Apply_ID").show();
                }
            },
            'input #Popup-Popup-color': function () {
                (graph.getCell($(popupView).attr('model-id'))).setColor($("#Popup-Popup-color")[0].value);
            },
            'click #Apply_ID': function () {
                ExportIDList.UpdateID((graph.getCell($(popupView).attr('model-id'))).getExportID(), $("#Popup-Popup-ID")[0].value);
                (graph.getCell($(popupView).attr('model-id'))).setExportID($("#Popup-Popup-ID")[0].value);
                cell.attributes.parentID = $("#Popup-Popup-Parent-ID")[0].value;
                $("#Apply_ID").hide();
            }
        },
        target: this
    })).render();
}

var floorSt = new Floor();
floorSt.resize(120, 120);
floorSt.setText("Floor");

var popupSt = new Popup();
popupSt.resize(120, 120);
popupSt.setText("Popup");

var serviceSt = new Service();
serviceSt.setText("Abc");

var textSt = new TextLabel();
textSt.setText("Text");

var svgimgSt = new SvgImage();
svgimgSt.setText("Abc");

var svgimgSt2 = new SvgImage2();
svgimgSt2.setText("Abc");

// Stencil is rendered in the DOM now.
$('#stencil-container').append(stencil.render().el);
stencil.load([serviceSt, floorSt, popupSt, textSt, svgimgSt, svgimgSt2]);

// Unhighlight embeddable elements on adding from stencil
let dropping = false;
$("#paper-container-zoomable").mousemove(function (event) {
    if (dropping) {
        graph.getElements().forEach(function (el) {
            paper.findViewByModel(el).unhighlight();
        });
    }
    if (event.which != 1) {
        dropping = false;
    }
});

function addEventsToNewElements() {
    $('.joint-cell').unbind("dblclick");
    $('.joint-cell').unbind("mousedown");

    $('.joint-type-service').on('dblclick', {
        iconsOptions: [
            { icon: 'icons/box.svg', content: 'Box', selected: true },
            { icon: 'icons/boxes.svg', content: 'Boxes' },
            { icon: 'icons/racks.svg', content: 'Racks' },
            { icon: 'icons/laptop.svg', content: 'Laptop' },
            { icon: 'icons/firewall.svg', content: 'Firewall' },
            { icon: 'icons/isp.svg', content: 'ISP' },
            { icon: 'icons/mpls.svg', content: 'MPLS' },
            { icon: 'icons/application.svg', content: 'Application' },
            { icon: 'icons/server.svg', content: 'Server' }
        ]
    }, showServicePopup);

    $('.joint-type-svgimage').on('dblclick', {
        iconsOptions: [
            { icon: 'icons/box.svg', content: 'Box', selected: true },
            { icon: 'icons/boxes.svg', content: 'Boxes' },
            { icon: 'icons/firewall.svg', content: 'Firewall' },
            { icon: 'icons/mpls.svg', content: 'MPLS' },
            { icon: 'icons/router.svg', content: 'Router' },
            { icon: 'icons/switch.svg', content: 'Switch' },
            { icon: 'icons/virtual-server.svg', content: 'Virtual Server' },
            { icon: 'icons/physical-server.svg', content: 'Phyical Server' }
        ]
    }, showServicePopup);

    $('.joint-type-svgimage2').on('dblclick', {
        iconsOptions: [
            { icon: 'icons/switch.svg', content: 'Switch', selected: true },
            { icon: 'icons/physical-server.svg', content: 'Phyical Server' }
        ]
    }, showServicePopup);

    $('.joint-type-floor').on('dblclick', showFloorPopup);
    $('.joint-type-popup').on('dblclick', showPopupPopup);
    $('.joint-type-textlabel').on('dblclick', showTextLabelPopup);
}

var selection = new Backbone.Collection();
var selectionView = new joint.ui.SelectionView({
    paper: paper,
    graph: graph,
    model: selection,
    type: 'toolbar',
    boxContent: false,
});

selectionView.removeHandle('rotate');
selectionView.addHandle({ name: 'bringtofront', icon: 'icons/send-to-front.svg' });
selectionView.addHandle({ name: 'sendtoback', icon: 'icons/send-to-back.svg' });

selectionView.on({
    'action:bringtofront:pointerdown': function (evt) {
        evt.stopPropagation();
        $.each(selection.models, function (index, value) { value.toFront({ deep: true }); });
    },
    'action:sendtoback:pointerdown': function (evt) {
        evt.stopPropagation();
        $.each(selection.models, function (index, value) { value.toBack({ deep: true }); });
    },
    'action:alignv:pointerdown': function (evt) {
        evt.stopPropagation();
        var averagex = 0;
        $.each(selection.pluck('position'), function (index, value) { averagex += value.x; });
        averagex /= selection.length;
        $.each(selection.models, function (index, value) { value.position(averagex, value.attributes.position.y); });
        $('.alignh').css('opacity', '0.25');
        $('.alignvv').css('opacity', '0.25');
        $('.alignv').css('opacity', '0.25');
    },
    'action:alignh:pointerdown': function (evt) {
        evt.stopPropagation();
        var averagey = 0;
        $.each(selection.pluck('position'), function (index, value) { averagey += value.y; });
        averagey /= selection.length;
        $.each(selection.models, function (index, value) { value.position(value.attributes.position.x, averagey); });
        $('.alignh').css('opacity', '0.25');
        $('.alignhh').css('opacity', '0.25');
        $('.alignv').css('opacity', '0.25');
    },
    'action:alignhh:pointerdown': function (evt) {
        evt.stopPropagation();
        var miny = Number.MAX_SAFE_INTEGER;
        var maxy = -Number.MAX_SAFE_INTEGER;
        $.each(selection.pluck('position'), function (index, value) {
            miny = miny > value.y ? value.y : miny;
            maxy = maxy < value.y ? value.y : maxy;
        });
        var step = (maxy - miny) / (selection.length - 1);
        miny -= step;
        $.each(selection.models, function (index, value) { value.position(value.attributes.position.x, miny += step); });
        $('.alignhh').css('opacity', '0.25');
    },
    'action:alignvv:pointerdown': function (evt) {
        evt.stopPropagation();
        var minx = Number.MAX_SAFE_INTEGER;
        var maxx = -Number.MAX_SAFE_INTEGER;
        $.each(selection.pluck('position'), function (index, value) {
            minx = minx > value.x ? value.x : minx;
            maxx = maxx < value.x ? value.x : maxx;
        });
        var step = (maxx - minx) / (selection.length - 1);
        minx -= step;
        $.each(selection.models, function (index, value) { value.position(minx += step, value.attributes.position.y); });
        $('.alignvv').css('opacity', '0.25');
    },
    'selection-box:pointerdown': function (evt) {
        if (evt.ctrlKey || evt.metaKey) {
            var cell = selection.get($(evt.target).data('model'));
            selection.reset(selection.without(cell));
            selectionView.destroySelectionBox(paper.findViewByModel(cell));
        }
    }
});

selection.on('reset add', function () {
    selectionView.removeHandle("alignv");
    selectionView.removeHandle("alignh");
    selectionView.removeHandle("alignvv");
    selectionView.removeHandle("alignhh");
    if (selection.length > 1 && selection.pluck('type').every((val, i, arr) => val === arr[0]) && selection.pluck('type')[0] == "Service") {
        selectionView.addHandle({ name: 'alignv', icon: 'icons/v.svg' });
        selectionView.addHandle({ name: 'alignh', icon: 'icons/h.svg' });
        selectionView.addHandle({ name: 'alignvv', icon: 'icons/vv.svg' });
        selectionView.addHandle({ name: 'alignhh', icon: 'icons/hh.svg' });
    }
});

// export to svg
function exportSVG() {
    var svgExportbbox = graph.getBBox(graph.getElements());
    if (!svgExportbbox) return;
    var svgForExport = paper.svg.cloneNode(true);
    // remove unnesessary elements from the final SVG
    $(svgForExport).find('.tool-remove').remove();
    $(svgForExport).find('.link-tools').remove();
    $(svgForExport).find('.marker-arrowheads').remove();
    $(svgForExport).find('.marker-source').remove();
    $(svgForExport).find('.marker-target').remove();
    $(svgForExport).find('.connection-wrap').remove();
    $(svgForExport).find('.marker-vertices').remove();
    $(svgForExport).find('.joint-tools-container').remove();
    svgForExport.setAttributeNS(null, 'viewBox', 0 + ' ' + 0 + ' ' + svgExportbbox.width + ' ' + svgExportbbox.height);
    svgForExport.querySelector('.joint-viewport').setAttributeNS(null, 'style', 'transform:scale(0.6)rotate(-30deg)skewX(30deg)scale(1,0.86602)translate(' + (-svgExportbbox.x) + 'px,' + (-svgExportbbox.y) + 'px);');
    svgForExport.setAttributeNS(null, 'preserveAspectRatio', 'xMidYMid meet');

    $.each($(svgForExport).find('text,tspan'), (i, el) => {
        if ($(el).html().replace(/&nbsp;/g, ' ') == noTextValue) {
            $(el).empty();
        }
    });

    $.each($(svgForExport).find('[export-id]'), function (i, e) {
        $(e).attr('id', $(e).attr('export-id'));
    });
    let showPopupScript = '\n\n//show popups scripts\n';
    $.each($(svgForExport).find('[model-id]'), function (i, e) {
        var model = graph.getCell($(e).attr('model-id'));
        if (model instanceof Service || model instanceof SvgImage || model instanceof SvgImage2) {
            e.id = model.attributes["export-id"];
        } else if (model instanceof Floor) {
            e.id = model.attributes["export-id"];
            $(e).attr('class', 'floor');
        } else if (model instanceof Link) {
            $(e).attr('id', null).attr('class', 'link');
            $(e).find('.connection')
                .attr('class', 'black_line')
                .attr('id', 'link-' + model.getSourceElement().getExportID() + '-' + model.getTargetElement().getExportID());
        } else if (model instanceof Popup) {
            e.id = model.attributes["export-id"];
            if (model.attributes.parentID) {
                let embedded = model.getEmbeddedCells({ deep: true });

                let idn = e.id.replace(/^service-/, '');
                $(e).attr('class', 'Popup');
                let g = document.createElementNS('http://www.w3.org/2000/svg', 'g');
                $(g).attr('id', 'Popup-' + idn);
                $('.joint-viewport', svgForExport).append(g);
                $('#Popup-' + idn, svgForExport).append(e);

                $.each(embedded, (im, em) => {
                    $('#Popup-' + idn, svgForExport).append($("[model-id='" + em.id + "']", svgForExport)[0]);
                });

                $('#Popup-' + idn, svgForExport)
                    .attr('class', 'hide');

                showPopupScript += `let timer${model.attributes.parentID};
                document.querySelector('#service-${model.attributes.parentID}')
                .addEventListener('mouseover', ()=>{
                    clearTimeout(timer${model.attributes.parentID});
                    document.querySelector('#Popup-${idn}').classList.add('show');
                    document.querySelector('#Popup-${idn}').classList.remove('hide');
                });
                document.querySelector('#service-${model.attributes.parentID}')
                .addEventListener('mouseleave', ()=>{
                    clearTimeout(timer${model.attributes.parentID});
                    timer${model.attributes.parentID} = setTimeout(()=>{
                        document.querySelector('#Popup-${idn}').classList.add('hide');
                        document.querySelector('#Popup-${idn}').classList.remove('show');
                    },2000);
                });
                document.querySelector('#Popup-${idn}')
                .addEventListener('mouseenter', ()=>{
                    clearTimeout(timer${model.attributes.parentID});
                    document.querySelector('#Popup-${idn}').classList.add('show');
                    document.querySelector('#Popup-${idn}').classList.remove('hide');
                });
                document.querySelector('#Popup-${idn}')
                .addEventListener('mouseleave', ()=>{
                    clearTimeout(timer${model.attributes.parentID});
                    timer${model.attributes.parentID} = setTimeout(()=>{
                        document.querySelector('#Popup-${idn}').classList.add('hide');
                        document.querySelector('#Popup-${idn}').classList.remove('show');
                    },2000);
                });`;
            } else {
                $(e).remove();
            }
        }
        if (model.attributes.reflected) {
            $($(e).find('image')).attr("class", "reflected");
        }
        $(e).removeAttr('model-id');
    });
    $.each($(svgForExport).find('.joint-cell'), function (i, e) {
        $(e).removeAttr('class');
    });
    $.each($(svgForExport).find('[data-type]'), function (i, e) {
        $(e).removeAttr('data-type');
    });
    $.each($(svgForExport).find('[joint-selector]'), function (i, e) {
        $(e).removeAttr('joint-selector');
    });

    var foundImgs = $(svgForExport).find('image');

    $.each(foundImgs, function (i, e) {
        let reflected = $(e).attr('class') == 'reflected';
        let cssTransform = reflected ?
            'scale(-0.86602, 1)skewX(30deg)rotate(-30deg)scale(0.8)translate(-36px,-15px)' :
            'scale(0.86602, 1)skewX(-30deg)rotate(30deg)scale(0.8)translate(36px,-15px)';
        var imgLink = $(e).attr('href');
        $.get(imgLink, function (result) {
            var g = document.createElementNS('http://www.w3.org/2000/svg', 'g');
            g.innerHTML = result.children[0].innerHTML;
            g.setAttributeNS(null, 'id', e.id);
            g.setAttributeNS(null, 'style', 'transform:' + cssTransform);
            // add hl-id inside image
            if ($(e).attr('class') == 'hl-icon') {
                let hl = $(g).find('[highlightable]');
                if (hl.length > 0) {
                    $(hl[0]).attr('id', 'hl-' + e.id.replace(/^icon-/, ''));
                    $(hl[0]).attr('style', null);
                    $(hl[0]).attr('highlightable', null);
                    $(hl[0]).attr('class', 'invisible');
                }
            }
            $(svgForExport).find('defs')[0].append(g);
            $(e).replaceWith(g);

            // after all 'href' replaced by symbols do
            if (foundImgs.length == i + 1) {
                $.when($.get('css/export_style.css', function (css) { })).done(function (css) {

                    let h = $(window).height() * 0.95;
                    let w = $(window).width() * 0.95;

                    $(function () {
                        $(".wrapper").append(`<div id="svg-code-edit">
                        <div id="svg-code-tabs">
                            <ul>
                                <li><a href="#svg-code-tab-1">SVG Code</a></li>
                                <li><a href="#svg-code-tab-2">CSS Code (embed into SVG)</a></li>
                                <li><a href="#svg-code-tab-3">JavaScript Code (embed into SVG)</a></li>
                            </ul>
                            <div id="svg-code-tab-1">
                                <div id="svg-editor" style="height:100vh;max-height:${h - 160}px;"></div>
                            </div>
                            <div id="svg-code-tab-2">
                                <div id="css-editor" style="height:100vh;max-height:${h - 160}px;"></div>
                            </div>
                            <div id="svg-code-tab-3">
                                <div id="js-editor" style="height:100vh;max-height:${h - 160}px;"></div>
                            </div>
                        </div>
                        <iframe id="svg-preview"></iframe>`);

                        $("#svg-code-edit").dialog({
                            title: () => "Export to SVG File".toUpperCase(),
                            //resizable: false,
                            autoOpen: false,
                            height: h,
                            width: w,
                            maxHeight: h,
                            maxWidth: w,
                            buttons: [
                                {
                                    text: "DOWNLOAD",
                                    click: function () {
                                        download(stringified, "edited-diagram.svg", "image/svg+xml");
                                        $(this).dialog("close");
                                    }
                                },
                                {
                                    text: "EXPORT TO DASHBOARD/IMAGES",
                                    click: function () {
                                        exportToServer(stringified, "edited-diagram.svg", "image/svg+xml");
                                       // $(this).dialog("close");
                                    }
                                },
                                {
                                    text: "CLOSE",
                                    click: function () {
                                        $(this).dialog("close");
                                    }
                                }
                            ],
                            create: function (event, ui) {
                                var widget = $(this).dialog("widget");
                                $(".ui-dialog-titlebar-close span", widget)
                                    .removeClass("ui-icon-closethick")
                                    .addClass("ui-icon-close");
                                initSVGEditor((new XMLSerializer()).serializeToString(svgForExport), css, embeddedSVGScript + showPopupScript);
                                $("#svg-code-tabs").tabs({
                                    heightStyle: "auto"
                                });
                            }
                        });
                        $("#svg-code-edit").dialog("open");
                    });
                });
            }
        });
    });
}

function exportToServer(SVGdata, filename = "diagram.svg", type = "image/svg+xml"){
    $(function () {
        $(".wrapper").append(`<div id="svgSaveDialog">
        <h5 style="margin: 0;">File Name:</h5>
        <input id="newFileName" style="width: 100%;" value="${filename}" />
        <div id="fileList">No projects found</div>
        </div>`);
        $("#svgSaveDialog").dialog({
            title: () => "Save the SVG".toUpperCase(),
            //resizable: false,
            autoOpen: false,
            modal: true,
            height: 600,
            width: 800,
            buttons: [
                {
                    text: "SAVE",
                    click: function () {
                        saveFile(true);
                        $(this).dialog("close");
                    }
                },
                {
                    text: "CLOSE",
                    click: function () {
                        $(this).dialog("close");
                    }
                }
            ],
            create: function (event, ui) {
                var widget = $(this).dialog("widget");
                $(".ui-dialog-titlebar-close span", widget)
                    .removeClass("ui-icon-closethick")
                    .addClass("ui-icon-close");

                widget.find("#fileList").fileTree({ root: "../../../dashboard/images/svgeditorexport/", script: "includes/jqueryfiletree/jqueryFileTree.php" }, function (file) {
                   console.log(file);
                });

                widget.find("#fileList").on('filetreeclicked', function (e, data) {
                    console.log(data);
                });
            }
        });
        $("#svgSaveDialog").dialog("open");
    });

    function saveFile(openMappingTool, serviceID = 1) {
        let data = new FormData();
        let filename = $('#newFileName')[0].value.replace(/\.svg$/i, '');
        data.append('filename', filename);
        data.append('data', SVGdata);
        data.append('mime_content_type', 'image/svg+xml');
        var xhr = new XMLHttpRequest();
        xhr.open('post', 'includes/jqueryfiletree/manageProjectFiles.php', true);
        xhr.send(data);
        if (openMappingTool){
            var win = window.open('/obsidian/dashboard/tab_mapping.php?file=svgeditorexport/'+filename+'&serviceID='+serviceID, '_blank');
            win.focus();
        }
    }
}

function download(data, filename = "diagram.svg", type = "image/svg+xml") {
    var file = new Blob([data], { type: type });
    if (window.navigator.msSaveOrOpenBlob) // IE10+
        window.navigator.msSaveOrOpenBlob(file, filename);
    else { // Others
        var a = document.createElement("a"),
            url = URL.createObjectURL(file);
        a.href = url;
        a.download = filename;
        document.body.appendChild(a);
        a.click();
        setTimeout(function () {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(url);
        }, 100);
    }
}

var $panzoom = $("#paper-container-zoomable").panzoom({
    cursor: "auto",
    which: 3,
    minScale: 0.25,
    startTransform: 'matrix(1, 0, 0, 1,' + ((-canvasWidth + $("#paper-container")[0].clientWidth) / 2) + ', ' + ((-canvasHeight + $("#paper-container")[0].clientHeight) / 2) + ')',
    maxScale: 1.75,
    // contain: true,
    $zoomIn: $('.btn-zoom-in'),
    $zoomOut: $('.btn-zoom-out'),
    $reset: $('.btn-zoom-reset'),
    // $zoomRange: $("input[type='range']")
});

// zoom by mouse wheel
$panzoom.parent().on('mousewheel.focal', function (e) {
    e.preventDefault();
    var delta = e.delta || e.originalEvent.wheelDelta;
    var zoomOut = delta ? delta < 0 : e.originalEvent.deltaY > 0;
    $panzoom.panzoom('zoom', zoomOut, {
        increment: 0.1,
        animate: false,
        focal: e
    });
});

//addEventsToNewElements();
//showHintDialog();

// sidebar events
// hide sidebar by clicking outside of it
$('section').click(function () { $('body').removeClass('offsidebar-open') });

function updateGraphView(id, value) {
    switch (id) {
        case 'gridSize':
            paper.setGridSize(value);
            break;
        case 'showGrid':
            if (value == 'y')
                paper.drawGrid();
            else
                paper.clearGrid();
            break;
        case 'showSnaplines':
            if (value == 'y') {
                snaplines.startListening();
            } else {
                snaplines.stopListening();
            }
            break;
        case 'primaryMeshThickness':
            // paper.options.drawGrid.args = [
            //         { color: 'red', thickness: value }, // settings for the primary mesh
            //         { color: 'green', scaleFactor: 15, thickness: 5 } //settings for the secondary mesh
            // ]
            break;
        case 'secondaryMeshThickness':

            break;
        case 'secondaryMeshScaleFactor':

            break;
        case 'showHint':
            if (value == 'y') {
                showHintDialog();
            }
            break;
        default:
            break;
    }
}
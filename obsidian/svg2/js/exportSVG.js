var stringified;
var mergedDoc;
var jsEditorValue = '';

function initJSCodeEditor(id, jscode=''){
    var jseditor;
    jseditor = ace.edit(id);
    jseditor.setTheme("ace/theme/chrome");
    jseditor.session.setMode("ace/mode/javascript");
    jseditor.on("input", updatePreview);
    jseditor.setValue(jscode);
    jseditor.clearSelection();

    preview = document.querySelector("#" + id);

    function updatePreview() {
        jsEditorValue = jseditor.getValue();
        preview.src = "data:application/javascript," + encodeURIComponent(jsEditorValue);
    }
}

function initSVGEditor(svgcode='', csscode='', jscode='') {
    var parser = new DOMParser();
    var svgeditor;
    var csseditor;
    var jseditor;
    var preview;

    svgeditor = ace.edit("svg-editor");
    svgeditor.setTheme("ace/theme/chrome");
    svgeditor.session.setMode("ace/mode/svg");
    svgeditor.on("input", updatePreview);

    csseditor = ace.edit("css-editor");
    csseditor.setTheme("ace/theme/chrome");
    csseditor.session.setMode("ace/mode/css");
    csseditor.on("input", updatePreview);

    jseditor = ace.edit("js-editor");
    jseditor.setTheme("ace/theme/chrome");
    jseditor.session.setMode("ace/mode/javascript");
    jseditor.on("input", updatePreview);
   
    preview = document.querySelector("#svg-preview");

    svgeditor.setValue(svgcode);
    csseditor.setValue(csscode);
    jseditor.setValue(jscode);

    svgeditor.clearSelection();
    csseditor.clearSelection();
    jseditor.clearSelection();
    //updatePreview();

    function updatePreview() {
        var code = svgeditor.getValue();
        var jscode = jseditor.getValue();
        var csscode = csseditor.getValue();

        var doc = parser.parseFromString(code, "image/svg+xml");

        mergedDoc = doc.querySelector('svg');

        var css = document.createElement("style");
        var js = document.createElement("script");

        css.setAttribute('class', 'custom-style');
        css.innerHTML = '\n' + csscode + '\n';

        js.setAttribute('class', 'custom-script');
        js.innerHTML = '\n' + '<![CDATA[\n' + jscode + '\n]]>' + '\n';

        mergedDoc.appendChild(css);
        mergedDoc.appendChild(js); 

        stringified = '<?xml version="1.0" encoding="utf-8"?>\n' + mergedDoc.outerHTML.replace(/&lt;/g, '<').replace(/&gt;/g, '>');

        preview.src = "data:image/svg+xml," + encodeURIComponent(stringified);
    }
}